﻿using System.Data.SqlClient;

namespace UlrikenLogger.Models
{
    public static class SqlConnectionFactory
    {
        public static SqlConnection Create(string server, string database, int timeout = 30)
            => new SqlConnection($"Database={database};Server={server};Integrated Security=True;connect timeout = {timeout}");
    }

}