﻿using Serilog.Core;
using Serilog.Events;
using System;

namespace UlrikenLogger.Logger.Components
{
    public class UlrikenActionSink : ILogEventSink
    {
        private IFormatProvider FormatProvider { get; }
        private Action<string, LogEventLevel> Action { get; }

        public UlrikenActionSink(Action<string, LogEventLevel> action, IFormatProvider formatProvider)
        {
            FormatProvider = formatProvider;
            Action = action;
        }

        public void Emit(LogEvent logEvent) => Action(logEvent.RenderMessage(FormatProvider), logEvent.Level);
    }
}
