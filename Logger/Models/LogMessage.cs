﻿using System;

namespace UlrikenLogger.Logger.Models
{
    public class LogMessage
    {
        public string ShortMessage { get; private set; }
        public string FullMessage { get; private set; }
        public object Data { get; private set; }
        public Exception Exception { get; private set; }
        public int LineNo { get; private set; }
        public string FileName { get; private set; }
        public SyslogLevel SyslogLevel { get; private set; }

        public static LogMessage Create(string shortMessage = "", string fullMessage = "", object data = null, Exception ex = null, int lineNo = 0, string fileName = "", SyslogLevel level = SyslogLevel.Debug) => new LogMessage
        {
            ShortMessage = shortMessage,
            FullMessage = fullMessage,
            Data = data,
            Exception = ex,
            LineNo = lineNo,
            FileName = fileName,
            SyslogLevel = level
        };

        public string ToString(bool simple = false, bool useDate = true)
            => useDate
                ? simple
                    ? $"{DateTime.Now}:{ShortMessage}:{FullMessage}"
                    : $"{DateTime.Now}:{SyslogLevel}:{FileName}:{LineNo}:{ShortMessage}:{FullMessage}:{Exception}"
                : simple
                    ? $"{ShortMessage}:{FullMessage}"
                    : $"{SyslogLevel}:{FileName}:{LineNo}:{ShortMessage}:{FullMessage}:{Exception}";
    }
}