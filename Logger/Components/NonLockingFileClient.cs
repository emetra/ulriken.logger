﻿using System;
using System.IO;
using UlrikenLogger.Logger.Models;
using UlrikenLogger.Services;
using UlrikenResult;
using UlrikenLogger.Helpers;

namespace UlrikenLogger.Logger.Components
{
    public class NonLockingFileClient : ILogClient
    {
        private static readonly object balanceLock = new object();
        private string FileName { get; }
        private bool Simple { get; }

        private Serilog.Core.Logger Log { get; }

        public NonLockingFileClient(string fileName, bool simple = false)
        {
            FileName = fileName;
            Simple = simple;
        }

        public void Dispose() { }

        public Result Send(LogMessage logMessage)
            => Do.Try(() =>
            {
                lock (balanceLock)
                {
                    File.AppendAllText(FileName,
                        $"{logMessage.SyslogLevel.AsLogEventLevel().AsString()}:{logMessage.ToString(Simple)}{Environment.NewLine}");
                }
            });
    }
}
