﻿using Serilog.Events;
using UlrikenLogger.Logger.Models;

namespace UlrikenLogger.Helpers
{
    public static class Extensions
    {
        public static LogEventLevel AsLogEventLevel(this SyslogLevel syslogLevel)
        {
            switch (syslogLevel)
            {
                case SyslogLevel.Alert: return LogEventLevel.Information;
                case SyslogLevel.Critical: return LogEventLevel.Fatal;
                case SyslogLevel.Debug: return LogEventLevel.Debug;
                case SyslogLevel.Emergency: return LogEventLevel.Fatal;
                case SyslogLevel.Error: return LogEventLevel.Error;
                case SyslogLevel.Informational: return LogEventLevel.Information;
                case SyslogLevel.Notice: return LogEventLevel.Information;
                case SyslogLevel.Warning: return LogEventLevel.Warning;
                default: return LogEventLevel.Debug;
            }
        }
        public static string AsString(this LogEventLevel @this)
        {
            switch (@this)
            {
                case LogEventLevel.Error: return "[ERR]";
                case LogEventLevel.Fatal: return "[FAT]";
                case LogEventLevel.Information: return "[INF]";
                case LogEventLevel.Verbose: return "[VER]";
                case LogEventLevel.Warning: return "[WRN]";
                case LogEventLevel.Debug: return "[DBG]";
                default: return "[DBG]";
            }
        }
    }
}
