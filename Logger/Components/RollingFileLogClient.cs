﻿using Serilog;
using Serilog.Core;
using Serilog.Events;
using UlrikenLogger.Logger.Models;
using UlrikenLogger.Services;
using UlrikenResult;
using UlrikenLogger.Helpers;

namespace UlrikenLogger.Logger.Components
{
    public class RollingFileLogClient : ILogClient
    {
        private bool Simple { get; }

        private Serilog.Core.Logger Log { get; }

        public RollingFileLogClient(
            string fileName,
            bool simple = false,
            RollingInterval rollingInterval = RollingInterval.Day,
            long fileSizeLimitBytes = 1024 * 1024,
            LogEventLevel logEventLevel = LogEventLevel.Information)
        {
            Simple = simple;
            Log = new LoggerConfiguration()
                .MinimumLevel.ControlledBy(new LoggingLevelSwitch { MinimumLevel = logEventLevel })
                .WriteTo.File(
                    fileName,
                    rollingInterval: rollingInterval,
                    rollOnFileSizeLimit: true,
                    fileSizeLimitBytes: fileSizeLimitBytes <= 0 ? 100 * 1024 * 1024 : fileSizeLimitBytes,
                    shared: true)
                .CreateLogger();
        }

        public void Dispose() { }

        public Result Send(LogMessage logMessage)
            => Do.Try(() => Log.Write(logMessage.SyslogLevel.AsLogEventLevel(), logMessage.ToString(Simple, false)));
    }
}
