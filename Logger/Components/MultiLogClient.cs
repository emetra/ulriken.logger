﻿using System.Collections.Generic;
using System.Linq;
using UlrikenLogger.Logger.Models;
using UlrikenLogger.Services;
using UlrikenResult;

namespace UlrikenLogger.Logger.Components
{
    public class MultiLogClient : ILogClient
    {
        private List<ILogClient> LogClients { get; }

        public MultiLogClient(List<ILogClient> logClients)
        {
            LogClients = logClients;
        }

        public void Dispose() { }

        public Result Send(LogMessage logMessage)
            => Do.Try(() => LogClients.ForEach(logClient => logClient.Send(logMessage)));

        public static ILogClient Create(params ILogClient[] logClients)
        {
            var list = new List<ILogClient>();
            list.AddRange(logClients.ToList());
            return new MultiLogClient(list);
        }
    }
}