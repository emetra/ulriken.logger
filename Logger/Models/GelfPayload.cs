﻿using Newtonsoft.Json;
using System;

// ReSharper disable InconsistentNaming

namespace UlrikenLogger.Logger.Models
{
    public class GelfPayload
    {
        public string version { get; set; }
        public string host { get; set; }
        public string short_message { get; set; }
        public string full_Message { get; set; }
        public long timestamp { get; set; }
        public int level { get; set; }
        public string facility { get; set; }
        public int line { get; set; }
        public string file { get; set; }
        public object _data { get; set; }
        public Exception _exception { get; set; }

        public override string ToString() => JsonConvert.SerializeObject(this);
    }
}
