﻿using System;
using UlrikenLogger.Logger.Models;
using UlrikenResult;

namespace UlrikenLogger.Services
{
    public interface ILogClient : IDisposable
    {
        Result Send(LogMessage logMessage);
    }
}
