﻿using System;
using UlrikenLogger.Logger.Models;
using UlrikenLogger.Services;
using UlrikenResult;


namespace UlrikenLogger.Logger.Components
{
    public class Log : ILog
    {
        #region Private properties
        private ILogClient LogClient { get; }

        #endregion

        public Log(ILogClient logClient)
        {
            Ensure.That(logClient).IsNotNull();
            LogClient = logClient;
        }

        public Result Result(Result result, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
                => result
                    .Then(res => LogClient.Send(LogMessage.Create(memberName, "Result returned SUCCESS", lineNo: sourceLineNumber, fileName: sourceFilePath)))
                    .Fail(res => LogClient.Send(LogMessage.Create(memberName, $"Result returned FAILED: {res.Message}", ex: res.Exception.ValueOrDefault(), lineNo: sourceLineNumber, fileName: sourceFilePath, level: SyslogLevel.Error)));

        public Result Info(string message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
            => LogClient.Send(LogMessage.Create(memberName, message, lineNo: sourceLineNumber, fileName: sourceFilePath, level: SyslogLevel.Informational));

        public Result Warning(string message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
            => LogClient.Send(LogMessage.Create(memberName, message, lineNo: sourceLineNumber, fileName: sourceFilePath, level: SyslogLevel.Warning));

        public Result Debug(string message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
            => LogClient.Send(LogMessage.Create(memberName, message, lineNo: sourceLineNumber, fileName: sourceFilePath));

        public Result Error(string message, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
            => LogClient.Send(LogMessage.Create(memberName, message, lineNo: sourceLineNumber, fileName: sourceFilePath, level: SyslogLevel.Error));

        public Result Error(Exception exception, string memberName = "", string sourceFilePath = "", int sourceLineNumber = 0)
            => LogClient.Send(LogMessage.Create(memberName, exception.Message, ex: exception, lineNo: sourceLineNumber, fileName: sourceFilePath, level: SyslogLevel.Error));

        public void Dispose()
        {
            LogClient?.Dispose();
        }
    }
}
