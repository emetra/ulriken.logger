﻿using Serilog;
using Serilog.Core;
using Serilog.Events;
using Serilog.Sinks.Graylog;
using UlrikenLogger.Logger.Models;
using UlrikenLogger.Services;
using UlrikenResult;
using UlrikenLogger.Helpers;

namespace UlrikenLogger.Logger.Components
{
    public class GrayLogClient : ILogClient
    {
        private bool Simple { get; }
        public void Dispose() { }
        private Serilog.Core.Logger Log { get; }

        public GrayLogClient(string host, bool simple = true, LogEventLevel logEventLevel = LogEventLevel.Information)
        {
            Simple = simple;
            Log = new LoggerConfiguration()
                .MinimumLevel.ControlledBy(new LoggingLevelSwitch { MinimumLevel = logEventLevel })
                .WriteTo.Graylog(new GraylogSinkOptions
                {
                    HostnameOrAddress = host,
                    Port = 12201
                })
                .CreateLogger();


        }

        public Result Send(LogMessage logMessage)
            => Do.Try(() => Log.Write(logMessage.SyslogLevel.AsLogEventLevel(), logMessage.ToString(Simple, false)));
    }
}
