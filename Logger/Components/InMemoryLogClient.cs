﻿using System.Collections.Generic;
using UlrikenLogger.Logger.Models;
using UlrikenLogger.Services;
using UlrikenResult;

namespace UlrikenLogger.Logger.Components
{
    public class InMemoryLogClient : ILogClient
    {
        public static List<LogMessage> Messages { get; set; }
        public void Dispose()
        {
            Messages = null;
        }

        public InMemoryLogClient(bool simple = true)
        {
            Messages = new List<LogMessage>();
        }
        public Result Send(LogMessage logMessage) => Do.Try(() => { Messages.Add(logMessage); });
    }
}
